import { Component, ViewChild, Inject } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { ScreenOrientation } from '@ionic-native/screen-orientation';

/** Firebase */
import * as firebase from 'firebase/app';
import { FirebaseApp } from "angularfire2";
import { AngularFireAuth } from "angularfire2/auth";

/** Paginas */
import { HomePage } from "../pages/home/home";
import { LoginPage } from "../pages/login/login";

/** Providers */
import { LocationTrackerProvider } from "../providers/location-tracker/location-tracker";
import {PushNotificationsProvider} from "../providers/push-notifications/push-notifications";
import {UserInfoProvider} from "../providers/user-info/user-info";

/**
 * Main component of the app.
 */
@Component({
  templateUrl: 'app.html'
})

export class MyApp {
  @ViewChild(Nav) nav: Nav;

  /** Reference to root page. */
  rootPage: any;

  /**
   * Constructor of the App component.
   *
   * @param firebaseApp Native plugin that works with push notifications, authentication, analytics, event tracking, crash reporting and more from Google Firebase.
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {StatusBar} statusBar Native component which manage the appearance of the native status bar.
   * @param {SplashScreen} splashScreen Native plugin which displays and hides a splash screen during application launch.
   * @param {LocationTrackerProvider} _locationTracker Custom provider for tracking background and foreground device geolocation.
   * @param {AngularFireAuth} _afAuth Firebase Authentication module
   * @param {PushNotificationsProvider} _pushNotif Custom provider to manage push notifications.
   * @param {UserInfoProvider} _userInfo Custom provider to create a customer object, set and obtain information.
   * @param {ScreenOrientation} screenOrientation Native plugin to set/lock the screen orientation in a common way.
   */
  constructor(
    @Inject(FirebaseApp) firebaseApp: any,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashScreen: SplashScreen,
    public _locationTracker: LocationTrackerProvider,
    public _afAuth: AngularFireAuth,
    public _pushNotif: PushNotificationsProvider,
    public _userInfo: UserInfoProvider,
    public screenOrientation: ScreenOrientation
  ) {

    this.initializeApp();
    const unsubscribe = firebase.auth().onAuthStateChanged(user => {
      if (!user) {
        this.rootPage = LoginPage;
        unsubscribe();
      } else {
        this.rootPage = HomePage;
        unsubscribe();
      }
    });
  }

  /**
   * Function called when the app launches. If the user is logged in it starts tracking geolocation, obtain the customer info from firebase and start open signal service to receive push notifications.
   * @returns void.
   */
  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
      if(this.platform.is("android")){
        this.screenOrientation.lock(this.screenOrientation.ORIENTATIONS.PORTRAIT);
      }
      this._afAuth.authState.subscribe(data => {
        if(data && data.uid && data.email){
          console.log("ID usuario: " + data.uid + " Email: " + data.email);
          console.log("start tracking");
          this._locationTracker.startTracking();
          this._userInfo.obtainUserInfoFromFirebase();
          this._pushNotif.init_notifications();
        }
      });
    });
  }

  /**
   * Function to navigate to another page passed by parameter.
   * @param pagina Page to navigate.
   * @returns void.
   */
  openPage(page) {
    this.nav.setRoot(page.component);
  }


}

import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { BackgroundGeolocation } from "@ionic-native/background-geolocation";
import { HttpClientModule } from "@angular/common/http";
import { GooglePlus } from "@ionic-native/google-plus";
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { CallNumber } from '@ionic-native/call-number';
import { OneSignal } from "@ionic-native/onesignal";
import { ScreenOrientation } from '@ionic-native/screen-orientation';

/** Paginas */
import { MyApp } from './app.component';
import { LoginPage } from "../pages/login/login";
import { RegisterPage } from "../pages/register/register";
import { HomePage } from "../pages/home/home";
import { LocationSelectPage } from "../pages/modals/location-select/location-select";
import { OpcionesCarreraPage } from "../pages/modals/opciones-carrera/opciones-carrera";
import { TaxiEncontradoPage } from "../pages/taxi-encontrado/taxi-encontrado";
import { OnTheWayPage } from "../pages/on-the-way/on-the-way";

/** Firebase*/
import { environment } from "../environments/environment";
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';

/** Providers */
import { TaxiDriverProvider } from '../providers/taxi-driver/taxi-driver';
import { CallProvider } from '../providers/call/call';
import { FormateoDatetimeProvider } from '../providers/formateo-datetime/formateo-datetime';
import { LocationTrackerProvider } from '../providers/location-tracker/location-tracker';
import { PushNotificationsProvider } from '../providers/push-notifications/push-notifications';
import { UserInfoProvider } from '../providers/user-info/user-info';
import { LoadingProvider } from '../providers/loading/loading';
import {FinalCarreraPage} from "../pages/final-carrera/final-carrera";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    RegisterPage,
    HomePage,
    LocationSelectPage,
    OpcionesCarreraPage,
    TaxiEncontradoPage,
    OnTheWayPage,
    FinalCarreraPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    AngularFireModule.initializeApp(environment.firebase),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpClientModule,
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    RegisterPage,
    HomePage,
    LocationSelectPage,
    OpcionesCarreraPage,
    TaxiEncontradoPage,
    OnTheWayPage,
    FinalCarreraPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    CallNumber,
    Geolocation,
    BackgroundGeolocation,
    ScreenOrientation,
    OneSignal,
      {provide: ErrorHandler, useClass: IonicErrorHandler},
    TaxiDriverProvider,
    CallProvider,
    FormateoDatetimeProvider,
    GooglePlus,
    LocationTrackerProvider,
    LocationTrackerProvider,
    PushNotificationsProvider,
    UserInfoProvider,
    LoadingProvider,
  ]
})

export class AppModule {}

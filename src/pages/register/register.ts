import { Component } from '@angular/core';
import {NavController, AlertController, ActionSheetController, ToastController} from 'ionic-angular';
import { HomePage } from "../home/home";
import {User} from "../../interfaces/user";
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

/**
 * Component of the Register page. It shows a form so the user can register with information such as name, lastname, email, profile photo, password, etc.
 */
@Component({
  selector: 'page-register',
  templateUrl: 'register.html',
})
export class RegisterPage {

  /** User object to set customer information. */
  user = {} as User;
  /** String of the confirmation password. */
  confirmation_password: string;

  /**
   * Constructor for the Register page.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {ActionSheetController} actionSheetCtrl Native component to access various features and information about the current view.
   * @param {AngularFireAuth} afAuth Firebase Authentication module
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   */
  constructor(
    private navCtrl: NavController,
    private alertCtrl: AlertController,
    private actionSheetCtrl: ActionSheetController,
    private afAuth: AngularFireAuth,
    private toast: ToastController
  ) {}

  /**
   * Function to register a new user with a confirmation password. It uses the Firebase authentication module.
   * @returns void.
   */
  register() {
    if (this.user.password != this.confirmation_password) {
      this.toast.create({
        message: 'Las contraseñas introducidas no coinciden',
        duration: 3000
      }).present();
    }
    else {
      this.afAuth.auth.createUserWithEmailAndPassword(this.user.email, this.user.password)
        .then( result => {
          console.log(this.user);
          this.navCtrl.push(HomePage);
          this.afAuth.authState.subscribe(data => {
            if(data && data.uid && data.email){
              this.toast.create({
                message: 'Bienvenido a Tacces, ¡pide tu taxi en 1 minuto!',
                duration: 3000
              }).present();

              //guardar datos usuario en firebase
              firebase.database()
                .ref('/users')
                .child(data.uid)
                .set({
                  name: this.user.name,
                  lastname: this.user.lastname,
                  email: data.email,
                  phone: this.user.phone,
                });
            }
          });
        })
        .catch( error => console.log("error al regisrar usuario" + error));
    }

  }

}

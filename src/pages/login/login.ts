import { Component } from '@angular/core';
import {NavController, AlertController, LoadingController, Loading, Platform, ToastController} from 'ionic-angular';
import { GooglePlus } from "@ionic-native/google-plus";

/** Interfaces */
import { User} from "../../interfaces/user";

/** Paginas */
import { HomePage } from "../home/home";
import { RegisterPage } from "../register/register";

/** Firebase */
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';

/**
 * The component for the LoginPage. It allows drivers to log in and reset password, but not to register (register manually to have more control for now).
 */
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  /** Loading component. */
  loading: Loading;
  /** User object with the information to log in. */
  user = {} as User;
  /** Animation in CSS trigerred when the app is loaded without being logged in. */
  splash: boolean = true;

/**
 * Constructor of the LoginPage
 * @param {NavController} navCtrl Base class to navigate between pages.
 * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
 * @param {LoadingController} loadingCtrl Default component which is an overlay that can be used to indicate activity while blocking user interaction.
 * @param {AngularFireAuth} afAuth Firebase Authentication module
 */
  constructor(private navCtrl: NavController,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private afAuth: AngularFireAuth,
  ) {}

  /**
   * Function called when loading the page and makes disappear the splashscreen animation after 4 seconds.
   * @returns void
   */
  ionViewDidLoad(){
    setTimeout( () => this.splash = false, 4000);
  }

  /**
   * Function that navigates to Register page.
   * @returns void.
   */
  public createAccount() {
      this.navCtrl.push(RegisterPage);
  }

  /**
   * Function that authenticates the user and redirects to the main screen or shows an error message.
   * @returns void
   */
  public login() {
      try{
        const result = this.afAuth.auth.signInWithEmailAndPassword(this.user.email, this.user.password)
          .then((user) => {
            this.navCtrl.setRoot(HomePage);
          })
          .catch(err=>{
            let alert = this.alertCtrl.create({
              title: 'Ups!',
              subTitle: "El usuario y/o contraseña inntroducidos no son correctos.",
              buttons: ['Aceptar']
            });
            alert.present();
          })
      }catch (e) {
        console.error(e);
      }
  }

  /**
   * Function to reset the password by sending an email to the user with the new password generated.
   * @returns void
   */
  resetPassword(){
    let prompt = this.alertCtrl.create({
      title: '¿Olvidaste la contraseña?',
      message: "Por favor, introduce tu correo electrónico y te enviaremos un email con un link para restaurar tu contraseña.",
      inputs: [
        {
          name: 'email',
          placeholder: 'Tu correo electrónico'
        },
      ],
      buttons: [
        {
          text: 'Solicitar nueva contraseña',
          handler: data => {
            firebase.auth().sendPasswordResetEmail(data.email);

          }
        },
        {
          text: 'Cancelar',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
      ]
    });
    prompt.present();
  }

}

import { Component, NgZone } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {ViewController} from 'ionic-angular';

declare var google: any;

/**
 * Component for the LocationSelect Page. It's a modal with a search bar where the user can search for places.
 */
@Component({
  selector: 'page-location-select',
  templateUrl: 'location-select.html',
})

export class LocationSelectPage {

  /** Array of possible addresses. */
  autocompleteItems: any[];
  /** Query for autocomplete. */
  autocomplete : any;
  /** Latitude coordinate. */
  latitude: number;
  /** Longitude coordinate. */
  longitude: number;
  geo: any;
  /** Google Maps Places AutoComplete service */
  service : any;

  /**
   * Constructor for the LocationSelect page
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed
   * @param {ViewController} viewCtrl Native component for accessing various features and information about the current view.
   * @param {NgZone} zone Angular NgZone. This special zone extends the basic functionality of a zone to facilitate change detection.
   */
  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl : ViewController, private zone : NgZone ) {
      this.service = new google.maps.places.AutocompleteService();
      this.autocompleteItems = [];
      this.autocomplete = {
          query: ''
      };
  }

  /**
   * Function triggered when the user modifies the input. Use the Google Maps Places AutoComplete service to get predictions about the address that the user is typing.
   * @returns void.
   */
  updateSearch() {
        console.log("entra");
        if (this.autocomplete.query == '') {
            this.autocompleteItems = [];
            return;
        }

        let me = this;
        this.service.getPlacePredictions({
            input: this.autocomplete.query,
            componentRestrictions: {
                country: 'es'
            }
        }, (predictions, status) => {
            me.autocompleteItems = [];
            me.zone.run(() => {
                if (predictions != null) {
                    predictions.forEach((prediction) => {
                        me.autocompleteItems.push(prediction.description);
                    });
                }
            });
        });
  }

  /**
   * Function triggered when the user select an address of the list of predictions and dismisses this modal.
   * @param item Object containing the information about the address chosen.
   * @returns void.
   */
  chooseItemAddress(item:any){
      this.viewCtrl.dismiss(item);
  }

  /**
   * Function that close this modal and send back to HomePage the address if the user has pressed 'OK' or nothing if the user has pressed 'Cancelar'
   * @param {boolean} borrar Boolean that determines one behaviour or another.
   * @returns void.
   */
  cerrarModalYBorrar(borrar: boolean){
    if(borrar) {
      this.viewCtrl.dismiss('delete');
    } else {
      this.viewCtrl.dismiss();
    }

  }
}

import { Component } from '@angular/core';
import {NavController, NavParams, AlertController, Alert, ViewController, DateTime} from 'ionic-angular';
import {CarreraOpciones} from "../../../interfaces/carrera-opciones";
import {FormateoDatetimeProvider} from "../../../providers/formateo-datetime/formateo-datetime";
import * as Constants from "../../../interfaces/constants";

/**
 * Component for the OpcionesCarrera page. It's a modal where the user can chose all the options of the service.
 */
@Component({
  selector: 'page-opciones-carrera',
  templateUrl: 'opciones-carrera.html',
})

export class OpcionesCarreraPage {

    /** Object with all the service options. */
    opciones : CarreraOpciones;
    /** Boolean that determines if the service pick up datetime is now or not. */
    recogerAhora: boolean;
    /** String of the pick up datetime. */
    fechaHoraRecogida : string;
    /** String of the payment. */
    payment : string;
    /** String of the driving type. */
    tipoConduccion : string;
    /** Boolean of premium driver. */
    taxiEstrella : boolean;
    /** Boolean of big car. */
    taxiGrande : boolean;
    /** String of minimum date of the datepicker component. */
    minDateOfPicker: string;
    /** String of maximum date of the datepicker component. */
    maxDateOfPicker: string;

  /**
   * Constructor of the OpcionesCarrera page.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {ViewController} viewCtrl Native component for accessing various features and information about the current view.
   * @param {FormateoDatetimeProvider} _formateoDatetime Custom provider for convert a string datetime into a Date variable or into a SQL date format (2018-12-04 12:00:00).
   */
  constructor(
      public navCtrl: NavController,
      public navParams: NavParams,
      public alertCtrl: AlertController,
      public viewCtrl: ViewController,
      public _formateoDatetime: FormateoDatetimeProvider
  ) {
      this.fechaHoraRecogida = new Date().toISOString();
      this.recogerAhora = true;
      this.payment = "efectivo";
      this.tipoConduccion = "normal";
      this.taxiEstrella = false;
      this.taxiGrande = false;
      this.inicializarDatePicker();
  }

  /**
   * Function to initialize the minimum and the maximum date that the user can book a service.
   * @returns void.
   */
  inicializarDatePicker(){
      let aDate= new Date();
      aDate.setHours(aDate.getHours() - (aDate.getTimezoneOffset() / 60));
      this.fechaHoraRecogida = aDate.toISOString();
      this.minDateOfPicker = aDate.toISOString();
      aDate.setDate(aDate.getDate() + 7);
      this.maxDateOfPicker = aDate.toISOString();
  }

  /**
   * Function to close this modal. Checks if the user has changed the pick up datetime and pass all data to the HomePage.
   * @returns void.
   */
  cerrarModalOpciones() {
    if((this._formateoDatetime.formattingToDate(this.fechaHoraRecogida).getTime()) - (new Date().getTime()) > Constants.FIVEMINUTESINMS){
        this.recogerAhora = false;
    }
    else{
        this.recogerAhora = true;
    }

    let data : any = {
        recogerAhora : this.recogerAhora,
        diaHoraRecogida : this.fechaHoraRecogida,
        metodoPago : this.payment,
        tipoConduccion : this.tipoConduccion,
        taxiEstrella : this.taxiEstrella,
        taxiGrande : this.taxiGrande,
    }
    console.log(data);
    this.viewCtrl.dismiss(data);
  }

  /**
   * Function that creates the alert for the payment and set it to the service options.
   * @returns void.
   */
  setPayment(){
      let alert = this.alertCtrl.create();
      alert.setTitle('Método de pago');

      alert.addInput({
          type: 'radio',
          label: 'Efectivo',
          value: 'efectivo',
          checked: true
      });
      alert.addInput({
          type: 'radio',
          label: 'Tarjeta',
          value: 'tarjeta',
          checked: false
      });

      alert.addButton('Cancel');
      alert.addButton({
          text: 'OK',
          handler: data => {
            this.payment = data;
              console.log(data)
          }
      });
      alert.present();
  }

  /**
   * Function that creates the alert for the drivin type and set it to the service options.
   * @returns void.
   */
  setVelocidad(){
      let alert = this.alertCtrl.create();
      alert.setTitle('Tipo de conducción');

      alert.addInput({
          type: 'radio',
          label: 'Lenta',
          value: 'lenta',
          checked: false
      });
      alert.addInput({
          type: 'radio',
          label: 'Normal',
          value: 'normal',
          checked: true
      });
      alert.addInput({
          type: 'radio',
          label: 'Rápida',
          value: 'rapida',
          checked: false
      });
      alert.addButton('Cancel');
      alert.addButton({
          text: 'OK',
          handler: data => {
              this.tipoConduccion = data;
              console.log(data)
          }
      });
      alert.present();
  }

}

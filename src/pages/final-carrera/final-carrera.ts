import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import {HomePage} from "../home/home";

/**
 * Component for the FinalCarrera page. It's used at the end of a service to thank the customer for using the app (TODO) and to rate the service.
 */
@Component({
  selector: 'page-final-carrera',
  templateUrl: 'final-carrera.html',
})
export class FinalCarreraPage {

  /**
   * Constructor of the FinalCarrera page
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   */
  constructor(public navCtrl: NavController) {}

  /**
   * Function to go back to HomePage.
   * @return void.
   */
  backToHomePage(){
    this.navCtrl.setRoot(HomePage);
  }



}

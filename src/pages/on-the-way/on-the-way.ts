import { Component, ElementRef, ViewChild } from '@angular/core';
import {
  DateTime,
  Loading,
  LoadingController,
  NavController,
  NavParams,
  Platform, ToastController,
} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import * as Constants from "../../interfaces/constants";
import * as firebase from 'firebase/app';

//providers
import { CallProvider } from "../../providers/call/call";

//interfaces
import { UserPosition } from "../../interfaces/user-position";
import {UserInfoProvider} from "../../providers/user-info/user-info";
import {TaxiDriver} from "../../interfaces/taxiDriver";
import {PushNotificationsProvider} from "../../providers/push-notifications/push-notifications";
import {HomePage} from "../home/home";
import {FinalCarreraPage} from "../final-carrera/final-carrera";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import {googleMapsStyle} from "../../environments/environment";

declare var google: any;

/**
 * Component for the OnTheWay page. It's the main page meanwhile the service is running. It shows information about the driver such as his name, phone and it also shows the location of the driver and the user's location.
 */
@Component({
    selector: 'page-on-the-way',
    templateUrl: 'on-the-way.html'
})
export class OnTheWayPage {

  /** Reference to the Google Maps html map. */
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  /** Driver object */
  driver = {} as TaxiDriver ;
  /** String of the service's deal id. */
  dealId: string;
  /** Array of markers for the customer and the driver. */
  markers: { role: string, marker: any } [] = [];

  /**
   * Constructor of the OnTheWay page.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {CallProvider} _call Custom provider for calling.
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {Geolocation} geolocation Native Cordova plugin for geolocation. Provides information about the device's location, such as latitude and longitude.
   * @param {UserInfoProvider} userInfo Custom provider for obtain and update the customer information from Firebase.
   * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed
   * @param {PushNotificationsProvider} _pushNotif Custom provider to manage push notifications.
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   */
  constructor(public navCtrl: NavController,
              public _call: CallProvider,
              public platform: Platform,
              public geolocation: Geolocation,
              public userInfo: UserInfoProvider,
              public navParams: NavParams,
              public _pushNotif: PushNotificationsProvider,
              public http: HttpClient,
              public toastCtrl: ToastController
  ){
    this.driver.uid = this.navParams.data.driverInfo.uid;
    this.driver.name = this.navParams.data.driverInfo.name;
    this.driver.phone = this.navParams.data.driverInfo.phoneNumber;
    console.log("DRIVER INFO " + JSON.stringify(this.navParams.data.driverInfo));
    console.log("PHONE -> " + JSON.stringify(this.driver.phone));
    this.dealId = this.navParams.data.dealId;
  }

  /**
   * Function triggered when the page is loaded. It loads the map and subscribes to an observable which especifies if the service has ended or the service is cancelled.
   * @returns void.
   */
  ionViewDidLoad() {
      this.loadMap();
      let obs = this._pushNotif.notificationObservable;
      obs.subscribe( value => {
        switch(value.tipo) {
          case Constants.NOTIFICATION_TYPE_WIN:
            this.navCtrl.push(FinalCarreraPage);
            break;
          case Constants.NOTIFICATION_TYPE_DRIVER_LEAVE:
            if(value.cancelService == true) {
              this.navCtrl.setRoot(HomePage);
            }
            break;
        }
      });
  }

  /**
   * Function that creates a map with the Google Maps API and also creates two markers with the position of the client and the driver in real time. Position coordinates are obtained from Firebase.
   * @returns void
   */
  loadMap() {
      this.platform.ready().then(() => {

          let latLngClient = new google.maps.LatLng(this.userInfo.user.location.lat, this.userInfo.user.location.lng);
          let mapOptions = {
              zoom: 16,
              tilt: 0,
              mapTypeId: google.maps.MapTypeId.ROADMAP,
              disableDoubleClickZoom: false,
              disableDefaultUI: true,
              zoomControl: false,
              scaleControl: false,
              styles: googleMapsStyle
          };
          this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

          //creamos el marcador provisional para el cliente
          let marker = new google.maps.Marker({
              position: latLngClient,
              icon: 'assets/imgs/user.png',
              animation: google.maps.Animation.BOUNCE,
          });
          this.markers.push({
              role : Constants.CLIENT_ROLE,
              marker : marker
          });

          let latLngDriver = new google.maps.LatLng(41.386028499999995,2.1059892);
          marker = new google.maps.Marker({
              position: latLngDriver,
              icon: 'assets/imgs/taxi16.png'
          });
          this.markers.push({
              role : Constants.DRIVER_ROLE,
              marker : marker
          });

          this.setMapOnAll(this.map);

          //se actualizan los marcadores cada vez que el cliente o el conductor se mueven
          firebase.database()
              .ref('/drivers')
              .child(this.driver.uid).on('value', data => {
              let driverLocation = new google.maps.LatLng(data.val().location.lat,data.val().location.lng);
              this.updateDriverMarker(Constants.DRIVER_ROLE, driverLocation);
              this.map.setCenter(new google.maps.LatLng(data.val().location.lat, data.val().location.lng));
          });

          firebase.database()
              .ref('/users')
              .child(this.userInfo.user.uid).on('value', data => {
              let userLocation = new google.maps.LatLng(data.val().location.lat,data.val().location.lng);
              this.updateDriverMarker(Constants.CLIENT_ROLE, userLocation);
          });
      });
  }

  /**
   * Function to update the coordinates of the customer or driver's markers
   * @param role A constant that it can be driver or client
   * @param position The coordinates of the marker
   */
  updateDriverMarker(role, position){
      this.markers.forEach( (element) => {
          if(element.role === role){
              element.marker.setPosition(position);
          }
      });
      this.setMapOnAll(this.map);
  }

  /**
   * Function to place all the markers of the array on the map
   * @param map The Google Maps map created
   * @returns void
   */
  setMapOnAll(map) {
      this.markers.forEach( (element) => {
          element.marker.setMap(map);
      })
  }

  /**
   * Function to call the driver using CallProvider
   * @returns void
   */
  llamarTaxista(){
    this._call.llamar(this.driver.phone);
  }

  /**
   * Function to cancel the service, make a post request to the server and navigate to home page.
   * @returns void.
   */
  cancelarServicio(){
    this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + "/cancel", null, {
      headers: new HttpHeaders().set('Content-Type', 'application/json')
    }).subscribe((res: any) => {
      this.navCtrl.push(HomePage);
      let toast = this.toastCtrl.create({
        message: 'Servicio cancelado.',
        showCloseButton : true,
        position: 'bottom'
      });
      toast.present();
    }, (err) => {
      console.log(err);
      let toast = this.toastCtrl.create({
        message: 'No se ha podido cancelar el servicio. Inténtalo de nuevo más tarde.',
        showCloseButton : true,
        position: 'bottom'
      });
      toast.present();
    });
  }

}

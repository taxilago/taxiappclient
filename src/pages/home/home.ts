import { Component, ElementRef, ViewChild } from '@angular/core';
import {
  DateTime,
  Loading,
  LoadingController,
  AlertController,
  Modal,
  ModalController,
  NavController,
  Platform,
  ToastController,
} from 'ionic-angular';
import { Geolocation } from '@ionic-native/geolocation';
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";

//providers
import { TaxiDriverProvider } from "../../providers/taxi-driver/taxi-driver";
import { FormateoDatetimeProvider } from "../../providers/formateo-datetime/formateo-datetime";
import { LocationTrackerProvider } from "../../providers/location-tracker/location-tracker";

//interfaces
import { UserPosition } from "../../interfaces/user-position";
import { Deal } from "../../interfaces/deal";
import { Servicio } from "../../interfaces/servicio";
import * as Constants from "../../interfaces/constants";

//paginas
import { LocationSelectPage } from "../modals/location-select/location-select";
import { OpcionesCarreraPage } from "../modals/opciones-carrera/opciones-carrera";
import { TaxiEncontradoPage } from "../taxi-encontrado/taxi-encontrado";
import {LoginPage} from "../login/login";

//firebase
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import {LoadingProvider} from "../../providers/loading/loading";
import {PushNotificationsProvider} from "../../providers/push-notifications/push-notifications";
import {OSNotification} from "@ionic-native/onesignal";
import {OnTheWayPage} from "../on-the-way/on-the-way";
import {googleMapsStyle} from "../../environments/environment";

declare var google: any;

/**
 * The component for the HomePage, the main page. It shows a map with the customer location and the markers of all the drivers using the app. It also shows a form to introduce all the service information. When the customer introduce an origin and a destination addresses a path is displayed on the map showing the route of the driver. It also shows the price and time of the service.
 */
@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {

  /** The reference of the map in the html. */
  @ViewChild('map') mapElement: ElementRef;
  map: any;
  /** Google Maps direction service */
  directionsService: any;
  /** Google Maps display service */
  directionsDisplay: any;
  /** Service object that contains all the information. */
  servicio: Servicio;
  /** Object with the position of the driver and the marker of the map */
  taxisPosition: { taxi: UserPosition, marker: any }[] = [];
  /** User position with latitude and longitude. */
  userPosition: { lat: number, lng: number };
  /** Loading component */
  requestTaxiAlert: Loading;
  /** String of the origin or destination address. */
  originOrDestinationInput: string;
  /** Marker for the origin coordinates. */
  originMarker: any;
  /** Marker for the destination coordinates. */
  destinationMarker: any;
  /** Marker for the user of the app. */
  userMarker: any;
  /** String of the id of the booking request. */
  requestId: any;
  /** String of the price of the service. */
  precio: string;
  /** String of the time of the service */
  tiempo: string;
  /** Boolean to manage if the price and time information about the service has to be displated. */
  mostrarPrecio: boolean;

  /**
   * Constructor of the HomePage. Some information of the service is set automatically.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {LoadingController} loadingCtrl Default component which is an overlay that can be used to indicate activity while blocking user interaction.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   * @param {Geolocation} geolocation Native Cordova plugin for geolocation. Provides information about the device's location, such as latitude and longitude.
   * @param {TaxiDriverProvider} _taxiDriverPrv Custom provider to get the location of all driver using the app from Firebase and keep it up to date on the map while they're driving.
   * @param {FormateoDatetimeProvider} _formateoDateTime Custom provider for convert a string datetime into a Date variable or into a SQL date format (2018-12-04 12:00:00).
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {ModalController} modalCtrl Default component which is a content pane that goes over the user's current page.
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   * @param {AngularFireAuth} afAuth Firebase Authentication module
   * @param {LocationTrackerProvider} _locationTracker Custom provider for tracking background and foreground device geolocation.
   * @param {LoadingProvider} _loading Custom provider for making a loading component which can be used from any page.
   * @param {PushNotificationsProvider} _pushNotif Custom provider to manage push notifications.
   */
  constructor(public navCtrl: NavController,
              public loadingCtrl: LoadingController,
              public alertCtrl: AlertController,
              public toastCtrl: ToastController,
              public geolocation: Geolocation,
              public _taxiDriverPrv: TaxiDriverProvider,
              public _formateoDateTime: FormateoDatetimeProvider,
              public platform: Platform,
              public modalCtrl: ModalController,
              public http: HttpClient,
              public afAuth: AngularFireAuth,
              public _locationTracker: LocationTrackerProvider,
              public _loading: LoadingProvider,
              public _pushNotif: PushNotificationsProvider
  ) {
    this.mostrarPrecio = false;
    this.directionsService = new google.maps.DirectionsService;
    this.directionsDisplay = new google.maps.DirectionsRenderer;

      this.servicio = {
          userId : this.afAuth.auth.currentUser.uid,
          pickupNow : true,
          origin : {
              lat : null,
              lng : null,
              address : ""
          },
          destination : {
              lat : null,
              lng : null,
              address : ""
          },
          pickupAt: "",
          bookingOptions : {
              payment : "efectivo",
              drivingType : "normal",
              premiumDriver: false,
              bigCar: false
          }
      };
  }

  /**
   * Function called when the app is launched. It loads the Google Maps map and set the pick up datetime of the service to current datetime-
   * @returns void.
   */
  ionViewDidLoad() {
    this.loadMap();
    let fechaActual = new Date();
    fechaActual.setHours(fechaActual.getHours()+2);
    this.servicio.pickupAt = this._formateoDateTime.formatingToServerFormat(fechaActual.toISOString());
  }

  /**
   * Function that loads the map and all the funcionalities related to: sets the input of the origin address with current location, create and set the marker for the user location, subscribe to Firebase drivers position, create those markers and set them into the map.
   * @returns void.
   */
  loadMap() {

    this.platform.ready().then(() => {

      let loading = this.loadingCtrl.create({
        content: 'Obteniendo ubicación...'
      });

      loading.present();

      this.geolocation.getCurrentPosition().then((position) => {
        loading.dismiss();

        let latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);

        //rellenamos el input de origen con la posicion actual y modificamos el driverInfo
        let geocoder = new google.maps.Geocoder();
        geocoder.geocode({ 'latLng': latLng }, (results, status) => {
          if (status == google.maps.GeocoderStatus.OK){
              this.servicio.origin.address = results[0].formatted_address;
          }
        });
        this.servicio.origin.lat = position.coords.latitude;
        this.servicio.origin.lng = position.coords.longitude;

        let mapOptions = {
          center: latLng,
          zoom: 17,
          tilt: 0,
          mapTypeId: google.maps.MapTypeId.ROADMAP,
          disableDoubleClickZoom: false,
          disableDefaultUI: true,
          zoomControl: false,
          scaleControl: false,
          styles: googleMapsStyle
        };

        this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);

        this.directionsDisplay.setMap(this.map);

        google.maps.event.addListenerOnce(this.map, 'idle', () => {

        this._taxiDriverPrv.driversLocationAdd.subscribe((driver: UserPosition) => {
          this.addUserMarker(driver)
            .then( marker => this.taxisPosition.push({taxi: driver, marker: marker}))
            .catch(error => console.log(error));
        });

        this._taxiDriverPrv.driversLocationUpdate.subscribe((driver: UserPosition) => {
          this.updateUserMarker(driver);
        });

        this._taxiDriverPrv.driversLocationRemove.subscribe((driver: UserPosition) => {
          this.deleteUserMarker(driver.id);
        });

        });

        //creamos e insertamos el marcador para el usuario

        this.userMarker = new google.maps.Marker({
          position: latLng,
          map: this.map,
          animation: google.maps.Animation.BOUNCE,
          icon: 'assets/imgs/user.png'
        });
        }, (err) => {
            console.log(err);
        });

        let watch = this.geolocation.watchPosition();
        watch.subscribe((data) => {
            let userLocation = new google.maps.LatLng(data.coords.latitude,data.coords.longitude);
            this.userMarker.setPosition(userLocation);
        });

    });
  }

  /**
   * Function to calculate the route and display the path between the origin and the destination points. It also calculate the duration of the service and call a function to calculate the price.
   * @param directionsService Google Maps direction service.
   * @param directionsDisplay Google Maps display service.
   * @returns void.
   */
  calculateAndDisplayRoute(directionsService, directionsDisplay) {

    directionsService.route(
      { origin: this.servicio.origin.address,
        destination: this.servicio.destination.address,
        travelMode: 'DRIVING' },
      (response, status)  => {
                if (status === 'OK') {
                  directionsDisplay.setDirections(response);
                  this.mostrarPrecio = true;
                  this.precio = this.calcularPrecio(response.routes[0].legs[0].distance.value);
                  this.tiempo = response.routes[0].legs[0].duration.text;
                } else {
                  console.log("Error intentando calcular la ruta.")
                }
        });
  }

  /**
   * Function to calculate the price of a service route.
   * @param {number} value The total of metres.
   * @returns {string} The price of the service.
   */
  private calcularPrecio(value: number) {
    let precio = parseFloat(Math.round(value/1000* 1.5).toFixed(2));
    if(precio <= 15) {
      return '15 €';
    }else {
      return precio + ' €';
    }
  }

  /**
   * Function to create a driver marker for the map.
   * @param {UserPosition} user UserPosition interface.
   * @returns {Promise<any>} Returns a promise to subscribe.
   */
  addUserMarker(user: UserPosition): Promise<any> {

    return new Promise((resolve, reject) => {
      let marker = new google.maps.Marker({
        map: this.map,
        title: 'id_taxista_1',
        icon: { 'url': 'assets/imgs/taxi16.png'},
        animation: google.maps.Animation.DROP,
        position: {
          lat: user.lat,
          lng: user.lng
        }
      });

      if(marker) {
        resolve(marker);
      } else {
        reject("No se ha podido crear el marker");
      }

    });
  }

  /**
   * Function to update a driver marker for the map.
   * @param {UserPosition} user UserPosition interface.
   * @returns void.
   */
  updateUserMarker(user: UserPosition) {
    this.taxisPosition.forEach( taxiPos => {
      if(taxiPos.taxi.id == user.id) {
        taxiPos.taxi = user;
        taxiPos.marker.setPosition({lat: user.lat, lng: user.lng});
      }
    })
  }

  /**
   * Function to delete a driver marker for the map.
   * @param {UserPosition} user UserPosition interface.
   * @returns void.
   */
  deleteUserMarker(id: string ) {
    this.taxisPosition.forEach( (taxiPos,index) => {
      if(taxiPos.taxi.id == id) {
        this.taxisPosition[index].marker.remove();
        this.taxisPosition.splice(index,1);
      }
    })
  }

  /**
   * Function to request a taxi. When the user pressed the button that trigger this function a post request is done. There's a loading component that keeps displaying meanwhile the Spring server finds the nearest free driver. When is found, the server sends a notification to the device and that loading component is dismissed. The aplication navigates to the next page.
   */
  requestTaxi(){

    let confirmar = this.alertCtrl.create({
      title : "Confirmación",
      message : "Estás seguro de que necesitas un taxi?",
      buttons: [
        {
            text: 'Cancelar',
            role: 'cancelar',
            handler: () => {
                console.log('Cancel clicked');
            }
        },
        {
            text: 'Confirmar',
            handler: () => {
              let loading = this.loadingCtrl.create({content: "Buscando taxista"});

              this._pushNotif.setLoader(loading);

              loading.onDidDismiss(() => {
                if(this._pushNotif.driverInfo != null) {
                  this.navCtrl.push(OnTheWayPage, { driverInfo: this._pushNotif.driverInfo, dealId: this._pushNotif.dealId });
                }
              });

              loading.present().then(() => {

                this.http.post(Constants.SERVER_IP + "/booking", JSON.stringify(this.servicio), {
                  headers: new HttpHeaders().set('Content-Type', 'application/json')
                }).subscribe((res: any) => {
                  this.requestId = res.id;
                }, (err) => {
                  loading.dismiss();
                  console.log(err);
                  let toast = this.toastCtrl.create({
                    message: 'No se ha podido solicitar un servicio. Inténtalo de nuevo más tarde.',
                    showCloseButton : true,
                    position: 'bottom'
                  });
                  toast.present();
                });

              });
            }
        }
      ]
    });

    confirmar.present();
  }

  /**
   * Function that creates and shows the modal for entering the origin or destination addresses. It uses the library places from Google Maps API to autocomplete the inputs. After the modal has dismissed the information is set to the service object.
   * @param {number} originOrDestination Constants that marks if the input is for origin address (1) or destination (2).
   * @returns void.
   */
  mostrarModalAddress(originOrDestination : number){
      let modal = this.modalCtrl.create( LocationSelectPage );
      modal.present();
      modal.onDidDismiss( parametros => {

          if(parametros != 'delete' && parametros){
            if(originOrDestination == 1){
              this.originOrDestinationInput = 'origin';
              this.servicio.origin.address = parametros;
              this.geoCode(parametros);
            }

            else if(originOrDestination == 2){
                this.originOrDestinationInput = 'destination';
                this.servicio.destination.address = parametros;
                this.geoCode(parametros);
            }

            if(this.servicio.origin.address != null && this.servicio.destination.address != null) {
              this.calculateAndDisplayRoute(this.directionsService, this.directionsDisplay);
            }

          }
          else if(parametros == 'delete'){
            this.mostrarPrecio = false;

            this.map.setCenter({lat: this.servicio.origin.lat, lng: this.servicio.origin.lng});
            this.map.setZoom(17);

            if(originOrDestination == 1) {

            } else if (originOrDestination == 2) {
              this.directionsDisplay.set('directions',null);
              this.servicio.destination.address = null;
            }
          }
      })
  }

  /**
   * Function to convert the address string into coordinates and add the markers to the map.
   * @param address String of the address
   * @returns void.
   */
  //convertir el string de la direccion en coords y posicionar el mapa
  geoCode(address:any) {
    let geocoder = new google.maps.Geocoder();
    geocoder.geocode({ 'address': address }, (results) => {
      this.map.setCenter(new google.maps.LatLng(results[0].geometry.location.lat(), results[0].geometry.location.lng()));
      if(this.originOrDestinationInput === 'origin'){
        // this.addOriginMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng());
      }
      else if(this.originOrDestinationInput === 'destination'){
        // this.addDestinationMarker(results[0].geometry.location.lat(), results[0].geometry.location.lng());
      }
    });
  }

  /**
   * Function to add origin marker into the map.
   * @param {number} latitude Latitude coordinate of the marker to be set.
   * @param {number} longitude Longitutde coordinate of the marker to be set.
   * @returns void.
   */
  addOriginMarker(latitude : number, longitude : number ){
    if(!this.originMarker){
      this.originMarker = new google.maps.Marker({
        map: this.map,
        title: this.originOrDestinationInput,
        icon: {'url': 'assets/imgs/marker.png'}, //TODO cambiar el icono por un taxi negro
        animation: google.maps.Animation.DROP, //TODO cambiar tipo de animaciÃ³n ?
        position: {
          lat: latitude,
          lng: longitude
        }
      });
    }
    else{
      this.originMarker.setPosition({lat: latitude, lng: longitude});
    }
  }

  /**
   * Function to add destination marker into the map.
   * @param {number} latitude Latitude coordinate of the marker to be set.
   * @param {number} longitude Longitutde coordinate of the marker to be set.
   * @returns void.
   */
  addDestinationMarker(latitude : number, longitude : number ){
    if(!this.destinationMarker){
      this.destinationMarker = new google.maps.Marker({
        map: this.map,
        title: this.originOrDestinationInput,
        icon: {'url': 'assets/imgs/marker.png'}, //TODO cambiar el icono por un taxi negro
        animation: google.maps.Animation.DROP, //TODO cambiar tipo de animaciÃ³n ?
        position: {
          lat: latitude,
          lng: longitude
        }
      });
    }
    else{
      this.destinationMarker.setPosition({lat: latitude, lng: longitude});
    }
  }

  /**
   * Function to create and show the modal for the service options (hour, payment, driving type, etc.). When the modal is dismissed the data is set into the service.
   * @returns void.
   */
  mostrarOpciones() {
    let modal = this.modalCtrl.create( OpcionesCarreraPage );
    modal.present();
    modal.onDidDismiss( parametros => {
      this.servicio.pickupNow = parametros.recogerAhora;
      this.servicio.pickupAt = this._formateoDateTime.formatingToServerFormat(parametros.diaHoraRecogida);
      this.servicio.bookingOptions.payment = parametros.metodoPago;
      this.servicio.bookingOptions.drivingType = parametros.tipoConduccion;
      this.servicio.bookingOptions.premiumDriver = parametros.taxiEstrella;
      this.servicio.bookingOptions.bigCar = parametros.taxiGrande;
    });
  }

  /**
   * Function to logout. It uses Firebase logout method, stops tracking geolocation and navigate to login page.
   * @returns void.
   */
  logout(){
    let alert = this.alertCtrl.create({
      title : "Salir",
      message : "Estás seguro de que deseas salir?",
      buttons: [
        {
          text: 'Cancelar',
          role: 'cancelar',
        },
        {
          text: 'Aceptar',
          handler: () => {
            firebase.auth().signOut();
            this.navCtrl.setRoot(LoginPage);
            this._locationTracker.stopTracking();
          }
        }
      ]
    });
    alert.present();
  }

}

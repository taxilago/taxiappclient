import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { CarreraOpciones } from "../../interfaces/carrera-opciones";
import { OnTheWayPage } from "../on-the-way/on-the-way";

/**
 * Component of the TaxiEncontrado page. Not used by now.
 */
@Component({
  selector: 'page-taxi-encontrado',
  templateUrl: 'taxi-encontrado.html',
})

export class TaxiEncontradoPage {
  /** CarreraOpciones object which contains booking options of the service. */
  opcionesCarrera: CarreraOpciones;

  /**
   * Constructor of TaxiEncontrado page.
   *
   * @param {NavController} navCtrl Base class to navigate between pages.
   * @param {NavParams} navParams Object that exists in the page and contain data from another view that it has passed
   */
  constructor(public navCtrl: NavController, public navParams: NavParams) {
      this.opcionesCarrera = navParams.get("opciones");
      console.log(this.opcionesCarrera);
  }

  /**
   * Function that navigates to OnTheWay page.
   * @returns void.
   */
  mostrarOnTheWay(){
      this.navCtrl.push( OnTheWayPage );
  }
}

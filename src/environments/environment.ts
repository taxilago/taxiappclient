export const environment = {
  production: false,
  firebase: {
    apiKey: "AIzaSyDimlAlx3yXT54wE3FCwrWJyuZmZTkh7Fo",
    authDomain: "apptaxiadaptat-bf3dc.firebaseapp.com",
    databaseURL: "https://apptaxiadaptat-bf3dc.firebaseio.com",
    projectId: "apptaxiadaptat-bf3dc",
    storageBucket: "apptaxiadaptat-bf3dc.appspot.com",
    messagingSenderId: "493329556287"
  }
};

export const googleMapsStyle =  [
  {
    "featureType": "all",
    "stylers": [
      {
        "saturation": 0
      },
      {
        "hue": "#e7ecf0"
      }
    ]
  },
  {
    "featureType": "road",
    "stylers": [
      {
        "saturation": -70
      }
    ]
  },
  {
    "featureType": "transit",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "poi",
    "stylers": [
      {
        "visibility": "off"
      }
    ]
  },
  {
    "featureType": "water",
    "stylers": [
      {
        "visibility": "simplified"
      },
      {
        "saturation": -60
      }
    ]
  }
];

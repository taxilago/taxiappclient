/**
 * UserPosition interface used in the map to manage driver's position. It contains information about location coordinates, last update, id and status.
 */
export interface UserPosition {
  lat: number,
  lng: number,
  lastUpdate: number,
  id?: string,
  status?: string
}

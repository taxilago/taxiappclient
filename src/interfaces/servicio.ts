import { Location } from "./location";

/**
 * Servicio interface with information about the service: userId, a user object with the information about the user sent by push notification, pickupNow (if the service datetime is between now and 15 minutes later), origin and destination location, pick up datetime, and the booking options (payment, conduction type, if a big car is needed or if the customer wants a premium driver).
 */
export interface Servicio{
    userId : string,
    pickupNow : boolean,
    origin : Location,
    destination : Location,
    pickupAt: string,
    bookingOptions : {
        payment : string,
        drivingType : string,
        premiumDriver: boolean,
        bigCar: boolean
    }
}

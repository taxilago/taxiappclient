/**
 * Interfaces for constants used in the whole app.
 */
export const FIVEMINUTESINMS = 7500000;

export const SERVER_IP: string = "http://192.168.43.83:8080";

export const CLIENT_ROLE: string = "client";
export const DRIVER_ROLE: string = "driver";

export const NOTIFICATION_TYPE_PENDINGS: string = "PENDINGS";
export const NOTIFICATION_TYPE_SCHEDULED: string = "SCHEDULED";
export const NOTIFICATION_TYPE_BOOKING: string = "BOOKING";
export const NOTIFICATION_TYPE_DRIVER: string = "DRIVER";
export const NOTIFICATION_TYPE_DRIVER_LEAVE: string = "DRIVER_LEAVE";
export const NOTIFICATION_TYPE_ON_DOOR: string = "ON_DOOR";
export const NOTIFICATION_TYPE_LOST: string = "LOST";
export const NOTIFICATION_TYPE_WIN: string = "WIN";

import {Location} from "./location";

/**
 * TaxiDriver interface with parameters: uid, name, lastname, email, phone, password, location and status.
 */
export interface TaxiDriver {
  uid: string,
  name: string,
  lastname: string,
  email: string,
  phone: string,
  password: string,
  location: Location
  status: number
}

/**
 * Location interface formed by latitude, longitude and a string with the address.
 */
export interface Location {
    lat: number,
    lng: number,
    address: string
}

/**
 * Deal interface used when a service is assigned to a driver.
 */
export interface Deal {
  id: string,

  dealDate: Date,
  dealTime: number,

  petitionId: string,
  driverId: string,

}

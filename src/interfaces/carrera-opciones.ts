/**
 * CarreraOpciones interface for manage service booking options in the app.
 */
export interface CarreraOpciones {
    diaHoraRecogida: Date,
    metodoPago: string,
    tipoConduccion: string,
    taxiEstrella : boolean;
    taxiGrande : boolean;
}

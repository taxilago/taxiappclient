import { Injectable } from '@angular/core';
import {LoadingController, LoadingOptions, App} from "ionic-angular";

/**
 * Provider for making a loading component which can be used from any page.
 */
@Injectable()
export class LoadingProvider {

  /** The loading component. */
  loading: any;

  /**
   * Constructor of the Loading provider.
   * @param {LoadingController} loadingCtrl Default component which is an overlay that can be used to indicate activity while blocking user interaction.
   */
  constructor(public loadingCtrl: LoadingController) {}

  /**
   * Function to create a loading component.
   * @param {LoadingOptions} loadingOptions Object with the options displayed on the loading component such as message, icon, timeout...
   * @returns void.
   */
  public getLoader(loadingOptions: LoadingOptions) : any {
    this.loading = this.loadingCtrl.create(loadingOptions).present();
  }

  /**
   * Function to dismiss the loading component.
   * @returns void.
   */
  public dismissLoader() {
    this.loading.dismiss();
  }


}

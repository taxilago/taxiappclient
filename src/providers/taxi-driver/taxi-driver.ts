import { Injectable } from '@angular/core';
import { Observable } from "rxjs/Observable";
import { UserPosition } from "../../interfaces/user-position";
import { AngularFireDatabase } from "angularfire2/database";
import "rxjs/add/operator/map";
import * as firebase from "firebase";
import DataSnapshot = firebase.database.DataSnapshot;

/**
 * Provider using Firebase database events of child added, changed or removed to monitor the location of all drivers using the app.
 */

@Injectable()
export class TaxiDriverProvider {

  /** Observable with the driver location when a child is added */
  public driversLocationAdd: Observable<UserPosition>;
  /** Observable with the driver location when a child is removed */
  public driversLocationRemove: Observable<UserPosition>;
  /** Observable with the driver location when a child is updated */
  public driversLocationUpdate: Observable<UserPosition>;

  /**
   * Constructor of TaxiDriver provider.
   *
   * @param {AngularFireAuth} _afb Firebase Authentication module
   */
  constructor(public _afb: AngularFireDatabase) {

    this.driversLocationAdd = Observable.create(observer => {
      firebase.database()
        .ref('/drivers')
        .on('child_added', (value: DataSnapshot) => {
          let driver = {} as UserPosition;

          driver.id = value.key;
          driver.lng = value.child('location').val().lng;
          driver.lat = value.child('location').val().lat;
          driver.status = value.val().status;

          observer.next(driver);
        });
    });

    this.driversLocationUpdate = Observable.create(observer => {
      firebase.database()
        .ref('/drivers')
        .on('child_changed', (value: DataSnapshot) => {
          let driver = {} as UserPosition;

          driver.id = value.key;
          driver.lng = value.child('location').val().lng;
          driver.lat = value.child('location').val().lat;
          driver.status = value.val().status;

          observer.next(driver);
        });
    });

    this.driversLocationRemove = Observable.create(observer => {
      firebase.database()
        .ref('/drivers')
        .on('child_removed', (value: DataSnapshot) => {
          let driver = {} as UserPosition;

          driver.id = value.key;
          driver.lng = value.child('location').val().lng;
          driver.lat = value.child('location').val().lat;
          driver.status = value.val().status;

          observer.next(driver);
        });
    });

  }

}

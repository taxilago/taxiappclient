import { Injectable } from '@angular/core';
import {OneSignal, OSNotification, OSNotificationPayload} from '@ionic-native/onesignal';
import {Platform, LoadingController, AlertController, NavController, App, ToastController} from "ionic-angular";
import * as Constants from "../../interfaces/constants";
import {LoadingProvider} from "../loading/loading";
import {constantCase} from "@ionic/app-scripts";
import {OnTheWayPage} from "../../pages/on-the-way/on-the-way";
import {Observable} from "rxjs/Observable";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Subject} from "rxjs/Subject";

/**
 * Provider for receiving and manage Push Notificacions using OneSignal.
 */
@Injectable()
export class PushNotificationsProvider {

    /** Loading component. */
    private loader;
    /** Object DriverInfo with the information about the driver assigned. */
    public driverInfo;
    /** Deal id of the service assigned. */
    public dealId;
    /** Observable to subscribe from another pages. */
    public notificationObservable = new Subject<any>();

  /**
   * Constructor of PushNotifications provider.
   * @param {OneSignal} oneSignal OneSignal native plugin which is an client implementation for using the OneSignal Service.
   * @param {Platform} platform Default service used to get information about the current device.
   * @param {LoadingController} loadingCtrl Default component which is an overlay that can be used to indicate activity while blocking user interaction.
   * @param {AlertController} alertCtrl Default component which is a dialog that presents users with information or collects information from the user using inputs.
   * @param {ToastController} toastCtrl Default component for subtle notification commonly used in modern applications. It can be used to provide feedback about an operation or to display a system message.
   * @param {LoadingProvider} _loading Custom provider to make a loading component which can be used from any page.
   * @param {HttpClient} http Cordova / Phonegap plugin for communicating with HTTP servers.
   */
    constructor(
        private oneSignal: OneSignal,
        private platform: Platform,
        private loadingCtrl: LoadingController,
        private alertCtrl: AlertController,
        public toastCtrl: ToastController,
        private _loading: LoadingProvider,
        private http: HttpClient
    ) {}

    /**
     * Main function to set up OneSignal and handle notifications received and opened.
     * @returns void.
     */
    init_notifications(){

        console.log("NOTIFICACION INICIALIZADA");
        if(this.platform.is("cordova")){

            this.oneSignal.startInit('176a6447-7609-42c7-93fe-a039605e8ffc', '534604254331');

            this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.Notification);

          console.log("ANTES DE HANDLENOTIFICATION");
            this.oneSignal.handleNotificationReceived()
                .subscribe((data: any) => {
                    console.log("NOTIFICATION RECIBIDA " + JSON.stringify(data));
                    this.onPushReceived(data.payload);
                });

            this.oneSignal.handleNotificationOpened()
                .subscribe( (data: any) => {
                    //this._loading.dismissLoader();
                    this.onPushOpened(data.payload);
                });

            this.oneSignal.endInit();
        }

        else{
            console.log("OneSignal no funciona en Chrome");
        }

    }

  /**
   * Function to assign a loading component created in any page to the loading of the provider to be managed in a specific notification.
   * @param loading Loading component.
   * @returns void.
   */
    public setLoader(loading) {
      this.loader = loading;
    }

    /**
     * Function to manage notifications when received. By now, there are 4 types of push notifications:
     * 1) Notification about the service has ended and the customer has arrived at the destination point.
     * 2) Notification with the information about the driver assigned to the service and the deal id.
     * 3) Notification about the driver is already at the pick up location.
     * 4) Notification the driver warned that is already at the pick up location and he's about to leave.
     * @param {OSNotificationPayload} payload Object received inside the notification push.
     * @returns void.
     */
    private onPushReceived(payload: OSNotificationPayload){

        switch (payload.additionalData.notificationType) {

          case Constants.NOTIFICATION_TYPE_WIN:
            let data = {
              tipo: Constants.NOTIFICATION_TYPE_WIN
            }
            this.notificationObservable.next(data);
            break;

          case Constants.NOTIFICATION_TYPE_DRIVER:
            if(this.loader != null) {
              this.loader.dismiss();
              this.driverInfo = payload.additionalData.data;
              console.log(JSON.stringify(this.driverInfo));
              this.dealId = payload.additionalData.data.dealId;
            }
            break;

          case Constants.NOTIFICATION_TYPE_ON_DOOR:
              this.alertCtrl.create({title: "Taxista en puerta!",
                buttons:
                  [{text: 'Aceptar'}]
              })
                .present();
            break;

          case Constants.NOTIFICATION_TYPE_DRIVER_LEAVE:
            let driverLeaveAlert = this.alertCtrl.create({
              title: "El taxista se va...",
              message: "En 45 segundos el servicio se cancelará automáticamente.",
              buttons:
                [
                  {
                    text: 'Espérame',
                    handler: () => {
                      clearTimeout(timer);
                      this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + '/not-leave/response', null, {
                        headers: new HttpHeaders().set('Content-Type', 'application/json')
                      }).subscribe((res: any) => {
                      }, (err) => {
                        console.log(err);
                        let toast = this.toastCtrl.create({
                          message: 'No se ha podido avisar al conductor de que te espere pero el servicio NO se ha cancelado.',
                          showCloseButton : true,
                          position: 'bottom'
                        });
                        toast.present();
                      });
                    }
                  },
                  {
                    text: 'Cancelar servicio',
                    handler: () => {
                      clearTimeout(timer);
                      this.http.post(Constants.SERVER_IP + "/deal/" + this.dealId + '/leave/response', null, {
                        headers: new HttpHeaders().set('Content-Type', 'application/json')
                      }).subscribe((res: any) => {
                        this.notificationObservable.next({tipo: Constants.NOTIFICATION_TYPE_DRIVER_LEAVE, cancelService: true});
                      }, (err) => {
                        console.log(err);
                        let toast = this.toastCtrl.create({
                          message: 'No se ha podido avisar al conductor de que ya no necesitas el taxi.',
                          showCloseButton : true,
                          position: 'bottom'
                        });
                        toast.present();
                      });
                    }
                  }
                ]
            });

            driverLeaveAlert.present();
            let timer = setTimeout(() => {
                driverLeaveAlert.dismiss();
                this.alertCtrl.create({title: "Servicio cancelado por tiempo de espera alcanzado.", buttons: [ {text: "Aceptar"}]}).present();
              },45000);
            break;

        }
    }

  /**
   * Unused function to handle notification when they're opened.
   * @param {OSNotificationPayload} payload Object received inside the notification push.
   * @returns void.
   */
    private onPushOpened(payload: OSNotificationPayload){
        let alert = this.alertCtrl.create({
            title: 'Informacion',
            message: payload.body,
        });
        alert.present();
    }

}

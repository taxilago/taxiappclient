import { Injectable } from '@angular/core';
import { AngularFireAuth } from "angularfire2/auth";
import * as firebase from 'firebase/app';
import {User} from "../../interfaces/user";

/**
 * Provider for obtain and update the customer information from Firebase.
 */
@Injectable()
export class UserInfoProvider {

  /** Customer object with all the information from database. */
  public user = {} as User;

  /**
   * Constructor of the UserInfo provider
   * @param {AngularFireAuth} afAuth Firebase Authentication module
   */
  constructor(private afAuth: AngularFireAuth) {
  }

  /**
   * Function to get current user information authenticated from Firebase and keep it up to date.
   * @returns void
   */
  obtainUserInfoFromFirebase (){
      this.user.uid = this.afAuth.auth.currentUser.uid;
      firebase.database()
          .ref('/users')
          .child(this.user.uid).on('value', data => {
          this.user.email = data.val().email;
          this.user.lastname = data.val().lastname;
          this.user.name = data.val().name;
          this.user.phone = data.val().phone;
          this.user.location = data.val().location;
      });
  }


}

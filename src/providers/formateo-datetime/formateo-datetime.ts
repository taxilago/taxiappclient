import { Injectable } from '@angular/core';

/**
 * Provider for convert a string datetime into a Date variable or into a SQL date format (2018-12-04 12:00:00).
 */
@Injectable()
export class FormateoDatetimeProvider {

  /**
   * Function to convert a String into a Date.
   * @param {string} fechaHora String of datetime to convert.
   * @returns {Date} Date obtained.
   */
  formattingToDate(fechaHora: string){
      let array = fechaHora.split("T");
      let fecha = array[0].split("-");
      let hora = array[1].split(".");
      hora = hora[0].split(":");

      let resultado = new Date();
      resultado.setFullYear(parseInt(fecha[0]));
      resultado.setMonth(parseInt(fecha[1])-1);
      resultado.setDate(parseInt(fecha[2]));
      resultado.setHours(parseInt(hora[0])+2);
      resultado.setMinutes(parseInt(hora[1]));
     return resultado;
  }

  /**
   * Function to format a datetime string (2018-12-04T12:00:00.263) into SQL date format (2018-12-04 12:00:00).
   * @param {string} fechaHora String of datetime to convert.
   * @returns {string} String obtained.
   */
  formatingToServerFormat(fechaHora: string){
    let resultado = fechaHora.substring(0, 19).replace("T", " ");
    return resultado;
  }

}

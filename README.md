# Tacces

App per a client i taxistes per demanar taxis adaptats. Aplicació orientada a clients amb mobilitat reduïda per poder demanar taxis adaptats. Permet a taxistes amb vehicles adaptats aumentar el seu volum de clients acceptant carreres que es publiquin a l'aplicació. Dues aplicacions desenvoluapdes, la de client i la de taxista, i el servidor que gestiona totes les peticions.

[Wireframe de les aplicacions](https://app.moqups.com/a16lydlaglag@iam.cat/HuwADF2N71/view)

## Manual d'instal·lació en entorn desenvolupament
1. En primer lloc es necessari instal·lar Node.js (8.11.2, la última versió estable).
> $ *curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -*
>> $ *sudo apt-get install nodejs*
2. Instal·lar GIT.
> $ *sudo apt install git*
>> $ *git config --global user.email "elteucorreu@gmail.com"*
>>> $ *git config --global user.name "El teu nom"*
3. Instal·lar Ionic.
> $ *npm install -g ionic*
4. Descarregar el codi dels repositoris de BitBucket de les dues aplicacions. 
> $ *git clone https://lydialago@bitbucket.org/lydialago/taxiappdriver.git  **o**  $ git clone https://lydialago@bitbucket.org/taxilago/taxiappclient.git*
5. Instal·lar totes les dependències del projecte.
> $ *npm install*
5. Compilar el projecte al navegador. 
> $ *ionic serve*

S’ha de tenir en compte, però que molts del plugins i funcionalitats de l’aplicació només funcionen en el dispositiu Android o iOS i no des del navegador, com per exemple les notificacions push o les peticions HTTP. Tot el fluxe per demanar taxi s'ha de probar amb les dues aplicacions corrents en dos dispositius.

### Desplegar l'aplicació en un dispositiu Android
Per desplegar l’aplicació en un dispositiu Android s'han de seguir aquest passos.

1. Instal·lar el Java Development Kit (JDK).
2. A continuació instal·lar Android Studio.
3. Des de Android Studio, descarregar el SDK per a la plataforma des de la qual volem desplegar l’aplicació (p.ex. Android 7.1.1 Nougat).
4. Des de l’arrel del projecte d’Ionic executar la següent comanda per crear un projecte Android amb tots el plugins.
> $ *ionic cordova platform add android*
5. Crear l’aplicació i obrir-la des de Android Studio.
> $ *ionic cordova build android*
6. A partir d’aqui, l’aplicació es pot desplegar en un emulador des de Android Studio o directament en el dispositiu. Des del menú *Tools > Android > ADV Manager* podem crear un emulador o desplegar l’aplicació directament en algun dispositiu que tinguem connectat per USB i en la mateixa xarxa. L’avantatge de fer servir Android Studio es que, a més a més, ens permet debuggar.
7. Una altra opció és des de linia de comandes executar la següent comanda assegurant-nos que el nostre dispositiu està connectat.
> $ *ionic cordova run android*